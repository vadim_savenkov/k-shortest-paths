import org.rdfhdt.hdt.dictionary.Dictionary;
import org.rdfhdt.hdt.exceptions.NotFoundException;
import org.rdfhdt.hdt.triples.IteratorTripleID;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;



interface GraphIndex {

    /**
     * TODO: comment the interface!
     * @param nodename
     * @param edge
     * @param target
     * @return
     */
    Collection<Integer> getOutlinks(int nodename, int edge, int target) throws NotFoundException;


    boolean validPath(ArrayList<Integer> oldPath, ArrayList<Integer> newPath) throws NotFoundException;


    int calculateNumberOfPaths(Set<List<Integer>> solutionpaths, int edge) throws NotFoundException;

    /**
     *
     * @param prevID
     * @param i
     * @param id
     * @return
     * @throws NotFoundException
     */
    IteratorTripleID lookUp(int prevID, int i, int id) throws NotFoundException;

    Dictionary getDict();

    int calcNumPaths(List<Integer> integers, int filterEdge) throws NotFoundException;
}
