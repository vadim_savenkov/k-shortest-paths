import org.rdfhdt.hdt.exceptions.NotFoundException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * Created by jumbrich on 01/06/16.
 */
public abstract class TopKAlgorithm implements  HDTInit{

    protected List<Path> solutions = new LinkedList<>();
    abstract public void init(Properties config) throws IOException;

    /**
     * Is called before each topk method call
     */
    abstract void reset();

    public List<String[]> topk(String start, String end, int k) throws Exception{
        System.out.println("TopK("+start+" , "+end+" , "+k+" )");

        reset();

        solutions = new LinkedList<>();

        return _topk(start, end, k);
    }

    abstract public List<String[]> _topk(String start, String end, int k) throws Exception;
    abstract public List<Path> run(String start, String end, String filter, int k) throws Exception;
}

interface HDTInit{
    public void init(HDTGraphIndex hdtIdx);
}