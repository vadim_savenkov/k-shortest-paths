import org.rdfhdt.hdt.triples.TripleID;

import java.util.*;

class Path implements Edges {
    protected ArrayList<Integer> vertSeq;
    protected int hash = 0;

    protected ArrayList<Integer> edgeSeq = null;

    public Path(){
        vertSeq = new ArrayList<>();
    }

    public Path(Collection<Integer> vertexSequence)
    {
        this();
        appendPath(vertexSequence);
    }

    public Path(Collection<Integer> vertexSequence, Collection<Integer> edgeSequence )
    {
        this();
        appendPath(vertexSequence,edgeSequence);
    }

    public Path( Path p ) {
        this();
        appendPath(p);
    }

    public void appendVertex(int vertex){
        vertSeq.add(vertex);
        if( edgeSeq != null ){
            throw new RuntimeException("Cannot add vertex without an edge to an edge-labeled path.");
        }
        hash = 0;
    }

    public void appendEdge( int edge, int vertex ) {

        if( edgeSeq == null && vertSeq.size()==1 ){
            edgeSeq = new ArrayList<>();
        }

        if( edgeSeq == null || edgeSeq.size()!= vertSeq.size()-1)
        {
            throw new RuntimeException(  "Number of edges " + (edgeSeq==null? 0 : edgeSeq.size())
                                       + " and vertices " + vertSeq.size() + " in an edge-labeled path do not match.");
        }
        edgeSeq.add(edge);
        vertSeq.add(vertex);
        hash = 0;
    }

    public void appendPath( Collection<Integer> vertexSequence ){
        appendPath(vertexSequence, null);
    }

    public void appendPath(Path path){
        appendPath(path.vertSeq, path.edgeSeq);
    }

    public void appendPath( Collection<Integer> vertexSequence, Collection<Integer> edgeSequence )
    {
        if( edgeSequence!=null )
        {
            if(edgeSeq==null){
                if( vertSeq.size()>1 ){
                    throw new RuntimeException("Appending labeled path to an unlabeled path");
                }
                edgeSeq = new ArrayList<>();
            }
            edgeSeq.addAll(edgeSequence);

            Iterator<Integer> vit = vertexSequence.iterator();
            if( !vertSeq.isEmpty() ) {
                // ensure that our last vertex and the first vertex in the appended path coincide
                if (  vit.hasNext() ) {
                    if( !vit.next().equals(vertSeq.get(vertSeq.size()-1)) ){
                        throw new RuntimeException("Only paths with matching last/first vertex can be concatenated");
                    }
                }
            }
            vertSeq.ensureCapacity( vertSeq.size() + vertexSequence.size() );
            while( vit.hasNext() ){
                vertSeq.add( vit.next() );
            }
            if( vertSeq.size() != edgeSeq.size()+1 ) {
                throw new RuntimeException(  "Number of edges " + (edgeSeq==null? 0 : edgeSeq.size())
                        + " and vertices " + vertSeq.size() + " in an edge-labeled path do not match.");
            }
        }
        else {
            if( isEdgeLabeled() ){
                if( vertexSequence.size()==1
                    && vertexSequence.iterator().next().equals(vertSeq.get(vertSeq.size()-1)) ){
                    //only this trivial case is allowed when we have edge labels and the appended path does not
                    return;
                }
                throw new RuntimeException("Appending an unlabeled path to a labeled path");
            }
            vertSeq.addAll(vertexSequence);
        }
        hash = 0;
    }

    public boolean isEdgeLabeled(){
        return edgeSeq!=null;
    }

    public boolean isValid(){
        if( isEdgeLabeled() ) {
            Set<PathEdge> edges = new HashSet<>(edgeSeq.size());

            for (Edge e : this.edgeSequence()) {
                PathEdge pe = new PathEdge(e);
                if (edges.contains(pe)) {
                    return false;
                }
                edges.add(pe);
            }
        }
        return true;
    }

    public void reverse(){
        Collections.reverse(vertSeq);
        if( edgeSeq != null ){
            Collections.reverse(edgeSeq);
        }
        hash=0; //set for rehash
    }

    @Override
    public int hashCode() {
        if(hash == 0) {
            hash = 1;
            for(Integer e : vertSeq) {
                if(e != null) {
                    hash = 5 * hash + e;
                }
            }
            if(edgeSeq!=null)
            {
                int edgeHash = 1;
                for(Integer e : this.edgeSeq) {
                    if(e != null) {
                        edgeHash = 5 * edgeHash + e;
                    }
                }
                hash += 100000*edgeHash + hash;
            }
        }
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if(o == this) {
            return true;
        }
        if(!(o instanceof Path) || this.hashCode()!=o.hashCode() ) {
            return false;
        }
        Path p = (Path)o;
        if( edgeSeq!= null && p.edgeSeq==null || !p.edgeSeq.equals(edgeSeq) ){
            return false;
        }
        return vertSeq.equals(p.vertSeq);
    }

    public int numVertices() {
        return vertSeq.size();
    }

    public boolean isEmpty(){
        return vertSeq.isEmpty();
    }

    public List<Integer> vertexList()
    {
        return Collections.unmodifiableList(vertSeq);
    }

    @Override
    public Iterable<Integer> vertexSequence() {
        return Collections.unmodifiableCollection(vertSeq);
    }

    @Override
    public Iterable<Edge> edgeSequence() {

        return new Iterable<Edge>() {
            @Override
            public Iterator<Edge> iterator(){
                return new EdgeIterator(vertSeq.iterator(), edgeSeq != null ? edgeSeq.iterator() : null);
            }
        };
    }

    @Override
    public String toString(){
        return Util.format(this);
    }

    class EdgeIterator implements Iterator<Edge>, Edge {

            final Iterator<Integer> v;
            final Iterator<Integer> e;

            int start = 0, end = 0, edge = 0;

            EdgeIterator( Iterator<Integer> vertexIterator, Iterator<Integer>  edgeIterator ) {
                v = vertexIterator;
                e = edgeIterator;

                if (v.hasNext()) {
                    end = v.next();
                }
            }

            @Override
            public boolean hasNext () {
                return v.hasNext();
            }

            @Override
            public Edge next () {
                start = end;
                end = v.next();
                if( e!=null ){
                    edge = e.next();
                }
                return this;
            }

            @Override
            public void remove () {
                v.remove();
                if( e!=null ){
                    e.remove();
                }
            }

            @Override
            public int start(){
                return start;
            }
            @Override
            public int edge(){
                return edge;
            }

            @Override public int end(){
                return end;
            }

    }

    public static class PathEdge implements Edge
    {
        int start;
        int edge;
        int end;

        public int start() { return start; }
        public int edge() { return edge; }
        public int end() { return end; }

        public PathEdge( Edge e ){
            this.start = e.start();
            this.edge = e.edge();
            this.end = e.end();
        }

        public PathEdge( TripleID t ){
            this.start = t.getSubject();
            this.edge = t.getPredicate();
            this.end = t.getObject();
        }

        @Override
        public int hashCode(){ return start + 1000*edge + 1000000 * end; }

        @Override
        public boolean equals( Object obj ){
            if( obj != null && obj.getClass().equals(PathEdge.class) ){
                PathEdge pe = (PathEdge)obj;
                return start == pe.start && edge == pe.edge && end == pe.end;
            }
            return false;
        }

    }

}
