/**
 * Created by vadim on 18.05.16.
 */
public interface Edge {
    int start();
    int edge();
    int end();
}
