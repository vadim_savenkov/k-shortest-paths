import org.rdfhdt.hdt.exceptions.NotFoundException;
import org.rdfhdt.hdt.triples.IteratorTripleID;
import org.rdfhdt.hdt.triples.TripleID;

import java.io.IOException;
import java.util.*;


public class BaselineBidirectionalTopK extends TopKAlgorithm implements  HDTInit{

    protected int startNode, targetNode, k;
    protected int filterEdge;
    protected HDTGraphIndex gIdx;

    final static int  FORWARD=0;
	final static int BACKWARD=1;

	Frontier[] frontier;

    @Override
    public void init(HDTGraphIndex hdtIdx) {
        gIdx=hdtIdx;
    }

    @Override
    public void init(Properties config) throws IOException {
        if(config.getProperty("type").equals("hdt")) {
            gIdx = new HDTGraphIndex(config.getProperty("dataset"), true);
        }
    }

    @Override
    void reset() {
        frontier = new Frontier[] {new Frontier(false), new Frontier(true)};
        solutions = new LinkedList<>();
    }

    @Override
    public List<String[]> _topk(String start, String end, int k) throws Exception {
        List<Path> res= run(start, end, null, k);

        return Util.formatToString(res,gIdx);
    }

    public List<Path> run(String start, String end, String filter, int k) throws Exception {
        startNode=gIdx.initLookUp(start,"subject");
        targetNode = gIdx.initLookUp(end, "object");
        filterEdge = filter==null? 0 : gIdx.initLookUp(filter, "predicate");

        reset();

        frontier[FORWARD].add( createFirstNode(false) );
        frontier[BACKWARD].add( createFirstNode(true) );

		int direction=FORWARD;

		while (!(frontier[FORWARD].isEmpty() || frontier[BACKWARD].isEmpty())) {

            frontier[direction] = frontier[direction].advance();

            //concatenate matching paths
            for( Integer i : frontier[FORWARD].vertices() ){
                //do we have any forward paths that satisfy the filter already?
                boolean hasFilteredPaths = frontier[FORWARD].hasPaths(i, true);
                // .. if not, all backward paths have to satisfy the filter. Otherwise, any will do
                if( frontier[BACKWARD].hasPaths(i, !hasFilteredPaths) ) {
                    // now go over all forward paths, satisfying the filter or not
                    for( PathDescriptor left : frontier[FORWARD].paths(i,false) ) {
                        left.path.reverse();
                        // if the forward path already satisfies the filter, concat with all backward paths,
                        // otherwise take only those backward paths that satisfy the filter
                        for ( PathDescriptor right : frontier[BACKWARD].paths(i, !left.filtered) ){
                            Path concat = new Path(left.path);
                            concat.appendPath(right.path);
                            if( concat.isValid() ) {
                                solutions.add(concat);
                            }
                        }
                    }
                }
            }
            if (solutions.size() >= k) {
                    return solutions;
			}
            direction = 1-direction;
        }
		return solutions;
	}

    static Path trace( TNode n, Path p ){
		if( n.prev == null ){
			return p;
		}
		p.appendEdge( n.prevEdge, n.prev.vertex );
		return trace(n.prev, p);
	}

    TNode createFirstNode(boolean backward){
        TNode n;
        if(backward){
            n = new BackwardTNode();
            n.init( targetNode, 0, null );
        }
        else{
            n = new ForwardTNode();
            n.init( startNode, 0, null );
        }
        n.filterEdge = filterEdge;
        return n;
    }

    TNode createNode(int vertex, int prevEdge, TNode prev, boolean backward){
        TNode n = backward? new BackwardTNode() : new ForwardTNode();
        n.init(vertex, prevEdge, prev);
        return n;
    }




    class Frontier{

        Map<Integer, List<TNode>> _q= new HashMap<>();
        final boolean backward;

        Frontier( boolean backward ){
            this.backward = backward;
        }

        void add(TNode n){
            if( n.isBackward()!=this.backward ){
                throw new RuntimeException("TNode direction does not match that of the frontier");
            }
            if ( !_q.containsKey(n.vertex)){
                _q.put(n.vertex, new LinkedList<TNode>());
            }
            _q.get(n.vertex).add(n);
        }

        Set<Integer> vertices(){ return _q.keySet(); }
        boolean hasPaths(Integer vertex, boolean checkFilter) {
            if( !checkFilter ) {
                return _q.containsKey(vertex);
            }
            if(_q.containsKey(vertex)) {
                for( TNode n : _q.get(vertex) ){
                    if( n.filterEdge == 0 ){
                        return true;
                    }
                }
            }
            return false;
        }

        List<PathDescriptor> paths( Integer vertex, boolean checkFilter ){
            List<PathDescriptor> paths = new ArrayList<>(_q.get(vertex).size());
            for( TNode node : _q.get(vertex) ){
                if( checkFilter && !node.satisfiesFilter() ){
                    continue;
                }
                Path p = new Path( Collections.singleton(vertex) );
                paths.add(new PathDescriptor(trace(node, p), node.satisfiesFilter()));
            }
            return paths;
        }

        Frontier advance(){
            Frontier f = new Frontier(this.backward);
            for( List<TNode> tnodes : _q.values() ){
                for(TNode node : tnodes ) {
                    for(TNode succ : node.successors() ) {
                        f.add(succ);
                    }
                }
            }
            return f;
        }

        boolean isEmpty(){ return _q.isEmpty(); }
    }

    abstract class TNode {
        int vertex;
        int prevEdge;
        int filterEdge = 0;
        TNode prev = null;

        void init( int vertex, int prevEdge, TNode prev ){
            this.vertex = vertex;
            this.prevEdge = prevEdge;
            this.prev = prev;
            if (prev != null) {
                filterEdge = prev.filterEdge;
                if (prev.prev == null && prevEdge == filterEdge) {
                    //filtering condition already satisfied
                    filterEdge = 0;
                }
            }
        }
        boolean satisfiesFilter(){ return filterEdge == 0; }
        abstract boolean isBackward();
        abstract boolean visitedEdge( TripleID t );
        abstract List<TNode> successors();
    }

    class ForwardTNode extends TNode
    {
        boolean isBackward(){ return false; }
        boolean visitedEdge(TripleID t) {
            if (prev != null) {
                    return vertex == t.getObject() && prevEdge == t.getPredicate()
                            && prev.vertex == t.getSubject() || prev.visitedEdge(t);
            }
            return false;
        }
        List<TNode> successors(){
            List<TNode> successors = new LinkedList<>();
            if( prev!=null && vertex > gIdx.getDict().getNshared() ){ // vertex encodes a literal, not an URI
                return successors;
            }
            try {
                 IteratorTripleID triples = gIdx.lookUp(vertex, 0, 0);
                  while (triples.hasNext()) {
                      TripleID t = triples.next();
                      if( !visitedEdge( t ) ) {
                          successors.add(createNode(t.getObject(), t.getPredicate(), this, false));
                      }
                  }
            }catch( NotFoundException ex ){}
            return successors;
        }
        @Override
        public String toString(){
            return prev==null? "("+vertex+")"
                    : String.format("(%s)-%s-(%s)", prev.vertex, prevEdge,vertex);
        }

    }

    class BackwardTNode extends TNode
    {
        boolean isBackward(){ return true; }
        boolean visitedEdge(TripleID t) {
            if (prev != null) {
                    return vertex == t.getSubject() && prevEdge == t.getPredicate()
                            && prev.vertex == t.getObject() || prev.visitedEdge(t);
            }
            return false;
        }
        List<TNode> successors(){
            List<TNode> successors = new LinkedList<>();
            try {
                IteratorTripleID triples = gIdx.lookUp(0, 0, vertex);
                while (triples.hasNext()) {
                    TripleID t = triples.next();
                    if( !visitedEdge( t ) ) {
                        successors.add(createNode(t.getSubject(), t.getPredicate(), this, true));
                    }
                }
            }catch( NotFoundException ex ){}
            return successors;
        }
        @Override
        public String toString(){
            return prev==null? "("+vertex+")"
                    : String.format("(%s)-%s-(%s)", vertex, prevEdge, prev.vertex);
        }
    }

    static class PathDescriptor{
        Path path;
        boolean filtered = false;
        PathDescriptor( Path path, boolean filtered ){
            this.path = path;
            this.filtered = filtered;
        }
    }
}