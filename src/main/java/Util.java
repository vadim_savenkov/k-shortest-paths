import org.rdfhdt.hdt.enums.TripleComponentRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by vadim on 18.05.16.
 */
public class Util
{

    static void printPaths(Collection<Path>paths, GraphIndex gi, String key){
        PrintWriter writer;
        try{
            File file = new File("./test_output/" + key);
            file.getParentFile().mkdirs();
            writer = new PrintWriter(new FileOutputStream(file, false));
        }
        catch(FileNotFoundException ex){ return; }

        for (Path path : paths) {
            writer.println(format(path,gi));
        }
        writer.close();
    }


    /**
     * Format a path using the integer-based representation.
     *
     * @param path A path to convert into string.
     * @return
     */
    public static String format(Path path){
        if( !path.isEdgeLabeled() ){
            return path.toString();
        }
        else{
            StringBuilder sb = new StringBuilder();
            for( Edge e : path.edgeSequence() ){
                if(sb.length()==0){
                    sb.append("[(");
                    sb.append(e.start());
                }
                sb.append(")-"); sb.append(e.edge()); sb.append("-(");
                sb.append(e.end());
            }
            sb.append(")]");
            return sb.toString();
        }
    }

    /**
     * Format a path, translating integers using the {@code Dictionary} of the {@code GraphIndex} object.
     *
     * @param path A path to convert into string.
     * @param gi {@code GraphIndex} for string conversion.
     * @return
     */
    public static String format(Path path, GraphIndex gi)
    {
        if(gi == null){ return format(path); }

        StringBuilder sb = new StringBuilder();
        org.rdfhdt.hdt.dictionary.Dictionary dict = gi.getDict();

        if( !path.isEdgeLabeled() ){
            sb.append('[');
            for(Integer v : path.vertexSequence()) {
                sb.append( trimNS(dict.idToString(v,TripleComponentRole.SUBJECT).toString()) );
                sb.append(",");
            }
            sb.setCharAt(sb.length()>1? sb.length()-1 : 1,']');
        }
        else{
            for( Edge e : path.edgeSequence() ){
                if(sb.length()==0){
                    sb.append("[(");
                    sb.append(trimNS(dict.idToString(e.start(),TripleComponentRole.SUBJECT).toString()));
                }
                sb.append(")-");
                sb.append(trimNS(dict.idToString(e.edge(),TripleComponentRole.PREDICATE).toString(), true));
                sb.append("-(");
                sb.append(trimNS(dict.idToString(e.end(),TripleComponentRole.OBJECT).toString()));
            }
            sb.append(")]");
            return sb.toString();
        }
        return sb.toString();
    }

    public static String getOrPutCache( Map<Integer,String> cache, org.rdfhdt.hdt.dictionary.Dictionary dict, TripleComponentRole role, Integer v){
        String s=cache.get(v);
        if(s==null){
            s=dict.idToString(v,role).toString();
            cache.put(v,s);
        }
        return s;
    }
    public static List<String[]> formatToString(Collection<Path> paths, GraphIndex gi){
        Map<Integer,String> scache = new HashMap<Integer, String>();
        Map<Integer,String> pcache = new HashMap<Integer, String>();
        org.rdfhdt.hdt.dictionary.Dictionary dict = gi.getDict();

        ArrayList<String[]> res= new ArrayList<String[]>();
        for(Path p: paths){

            ArrayList<String>  sb= new ArrayList<String>();
            if( !p.isEdgeLabeled() ){
                for(Integer v : p.vertexSequence()) {
                    sb.add( getOrPutCache(scache,dict,TripleComponentRole.SUBJECT,v) ) ;
                }
            }
            else{
                for( Edge e : p.edgeSequence() ){
                    if(sb.size()==0)
                        sb.add( getOrPutCache( scache, dict, TripleComponentRole.SUBJECT, e.start() ) ) ;
                    sb.add( getOrPutCache( pcache, dict, TripleComponentRole.PREDICATE, e.edge() ) ) ;
                    sb.add( getOrPutCache( scache, dict, TripleComponentRole.OBJECT, e.end() ) ) ;
                }
            }
            res.add(sb.toArray(new String[sb.size()]));
        }
        return res;
    }



    /**
     * Trim long namespaces at RDF IRIs.
     * @param URI
     * @param soft keep the last part of the URI (namespace): makes sense for some ontologies
     *        having properties differing in namespaces but not in local names.
     * @param ignore ignore the operation altogether (makes the calling code cleaner)
     * @return local URI
     */
    public static String trimNS( String URI, boolean soft, boolean ignore ){
        if( ignore ){ return URI; }
        int trimPos = URI.lastIndexOf('#');
        if( trimPos <= 0 ) {
            trimPos = URI.lastIndexOf('/');
        }
        if( soft && trimPos > 0 ){
            int penultimateSlash = URI.substring(0,trimPos-1).lastIndexOf('/');
             trimPos = penultimateSlash > 0 ? penultimateSlash : 0;
        }
        return trimPos>0? URI.substring(trimPos+1) : URI;
    }

    public static String trimNS( String URI, boolean soft ){
        return trimNS(URI,soft, false);
    }

    public static String trimNS( String URI ){
        return trimNS(URI,false);
    }



}
