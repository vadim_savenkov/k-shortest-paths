import org.rdfhdt.hdt.exceptions.NotFoundException;
import org.rdfhdt.hdt.triples.IteratorTripleID;
import org.rdfhdt.hdt.triples.TripleID;

import java.io.IOException;
import java.util.*;

public class BaselineTopKPath extends TopKAlgorithm implements  HDTInit{

	LinkedList<TNode> q;
	boolean reversed = false;
	protected int startNode, targetNode;
	protected int filterEdge;
	protected HDTGraphIndex gIdx;

	@Override
	public void init(Properties config) throws IOException {
        if(config.getProperty("type").equals("hdt"))
            gIdx = new HDTGraphIndex(config.getProperty("dataset"), true);
	}

    public void init(HDTGraphIndex hdtIdx) {
        gIdx=hdtIdx;
    }

    public void reset(){
		solutions = new LinkedList<>();
    }


	public List<String[]> _topk(String start, String end, int k) throws Exception {
		List<Path> results = run(start, end, null, k);
		return Util.formatToString(results,gIdx);
	}

	public List<Path> run(String start, String end, String filter,  int k) throws Exception {

        startNode = gIdx.initLookUp(start,"subject");
		targetNode = gIdx.initLookUp(end, "object");
		filterEdge = filter==null? 0 : gIdx.initLookUp(filter, "predicate");

		TNode n = TNode.createNode(startNode, 0, null);
		q = new LinkedList<>();
		q.add(n);


		int qsize = 0;
		while (!q.isEmpty()) {
			if (qsize < q.size()) {
				qsize = q.size();
			}
			TNode current = q.poll();
			visit(current);
			if (solutions.size() >= k) {
				System.out.println("Max queue size: " + qsize);
				break;
			}
		}
		return solutions;


	}

	private void visit(TNode current) throws NotFoundException {
		IteratorTripleID triples = gIdx.lookUp(current.vertex, 0, 0);

		TNode next = null;
		while (triples.hasNext()) {
			TripleID t = triples.next();

			if (current.visitedEdge(t)) {
				continue;
			}
			boolean doNotQueue = false;
			if (t.getObject() > gIdx.getDict().getNshared()) { // data objects are leaves
				doNotQueue = true;
				if (t.getObject() != targetNode) {
					continue;
				}
			}
			next = TNode.createNode(t.getObject(),t.getPredicate(),current);

			if (!doNotQueue) {
				q.add(next);
			}

			if (t.getObject() == targetNode && (current.filterEdge == 0 || t.getPredicate() == filterEdge)) {
				Path p = trace(next, new Path(Collections.singleton(targetNode)));
				if(!reversed){ p.reverse(); }
				solutions.add(p);
			}
		}

	}

	static Path trace( TNode n, Path p ){
		if( n.prev == null ){
			return p;
		}
		p.appendEdge( n.incomingEdge, n.prev.vertex );
		return trace(n.prev, p);
	}



    static class TNode {
		int vertex;
		int incomingEdge;
		int filterEdge = 0;
		TNode prev = null;

		TNode(int vertex, int incomingEdge, TNode prev) {
			this.vertex = vertex;
			this.incomingEdge = incomingEdge;
			this.prev = prev;
			if (prev != null) {
				filterEdge = prev.filterEdge;
				if (prev.prev == null && incomingEdge == filterEdge) {
					//filtering condition already satisfied
					filterEdge = 0;
				}
			}
		}

		static TNode createNode(int vertex, int incomingEdge, TNode prev){
			return new TNode(vertex,incomingEdge, prev);
		}

		boolean visitedEdge(TripleID t) {
			if (prev != null) {
				return vertex == t.getObject() && incomingEdge == t.getPredicate()
						&& prev.vertex == t.getSubject() || prev.visitedEdge(t);
			}
			return false;
		}
		public String toString(){
			return prev==null? "("+vertex+")"
							  : String.format("(%s)-%s-(%s)", prev.vertex, incomingEdge,vertex);
		}
	}

}
