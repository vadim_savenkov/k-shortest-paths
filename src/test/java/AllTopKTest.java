import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;


import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *  Use strings to specify graphs:
 *  s is assumed to be the start node, t - the end node,
 *  all other nodes are named arbitrarily.
 *   (+ "t_end" is internally appended to t to make it a subject)
 *
 *  Graph can be given as a set of paths combined by dots:
 *   "s-t" : path from s to t
 *   "s-a-t. s-b-t" : fork from s to a and to b, then join at t
 *   etc.
 *
 *
 * Created by Vadim on 13.05.2016.
 */
@RunWith(value = Parameterized.class)
public class AllTopKTest {


    private final Class topKClass;
    private TopKAlgorithm topK;

    public AllTopKTest(Class topKClass ){
        this.topKClass = topKClass;
    }

    @Parameters(name = "{0}")
    public static List<Object[]> getParameters() {
        List<Object[]> params = new ArrayList<>();
        // Build your list of parameters somehow.
        params.add(new Object[] { BaselineBidirectionalTopK.class });
        params.add(new Object[] { BaselineTopKPath.class });
        return params;
    }


    public void printSolutions(List<Path> solutions, GraphIndex gi){
        System.out.println("Found the following solutions");

        for(Path p: solutions)
            System.out.println(Util.format(p, gi));
    }

    @Before
    public void setUpTest() throws Exception{
        topK = (TopKAlgorithm) topKClass.newInstance();
    }

    @Test
    public void one_hop_path() throws Exception {
        //single hop from s to t
        TestUtil.Graph gi = new TestUtil.Graph("s-t",1);

        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);

        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }

    @Test
    public void two_hop_path() throws Exception {

        //single three-hop path from s to t
        TestUtil.Graph gi = new TestUtil.Graph("s-a1-t",1);

        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }



    @Test
    public void three_hop_path() throws Exception {

        //single three-hop path from s to t
        TestUtil.Graph gi = new TestUtil.Graph("s-a1-a2-t",1);
        //TestUtil.Graph gi_reverse = new TestUtil.Graph("t-a2-a1-s",1, true);

        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);
        assertEquals(gi.numpaths, solutions.size());
    }


    @Test
    public void simple_fork() throws Exception {

        //two two-hop paths from s to t, one via b1, another via b2
        String sPattern = "s-b1-t. s-b2-t";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,2);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }

    @Test
    public void complex1_paths() throws Exception {

        //two two-hop paths from s to t, one via b1, another via b2
        String sPattern = "s-b1-c1. s-b2-c1. s-b3-c1. c1-b4-t. c1-b5-t. c1-b6-t.";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,9);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }

    @Test
    public void complex2_paths() throws Exception {
        // Currently fails, since we remove node 9=c1, instead of following


        //two two-hop paths from s to t, one via b1, another via b2
        String sPattern = "s-b1-c1. s-b2-c1. s-b3-c1. c1-b4-t. c1-b5-t. c1-b6-t. s-b7-b8-c1.";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,12);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);
        assertEquals(gi.numpaths, solutions.size());
    }


    @Test
    public void fork_at_first_hop() throws Exception {

        //fork from s to d1 + d2, then join @c, proceed to t
        String sPattern = "s-d1-c-f-t. s-d2-c";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,2);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);
        assertEquals(gi.numpaths, solutions.size());
    }


    @Test
    public void fork_at_second_hop() throws Exception {

        //from s to f, then fork to g1 + g2, then join @t
        String sPattern = "s-f-c-g1-t. c-g2-t";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,2);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }


    @Test
    public void two_forks() throws Exception {

        //two consecutive fork&joins: s(h1|h2)c(i1|i2)t
        String sPattern = "s-h1-c-i1-t. s-h2-c-i2-t";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,4);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }

    @Test
    public void premature_stop() throws Exception
    {
        String sPattern = "s-j1-j2-t . s-k1-k2-k3-t. k2-j1";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,3);

        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }

    @Test
    public void loop() throws Exception {

        String sPattern = "s-s-t";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,2);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }

    @Test
    public void cycles() throws Exception {

        //an s-l-s loop + direct s-t path
        // s-t; s-l-s-m-s-t; s-m-s-l-s-t should be among possible paths
        String sPattern = "s-l-s-m-s-t";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,3);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }


    @Test
    public void cycle_plus_two_joins() throws Exception {
        //an s-n3-s loop + two consecutive fork&joins: s(n1|n2)c(p1|p2)t
        String sPattern = "s-n3-s. s-n1-c-p1-t. s-n2-c-p2-t";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,8);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }

    @Test
    public void challenge_example() throws Exception {

        String sPattern = "s-b-t. s-a-b.t";
        TestUtil.Graph gi = new TestUtil.Graph(sPattern,2);
        topK.init(gi.hdt);
        List<Path> solutions = topK.run(gi.start, gi.target, gi.edge, gi.numpaths);
        printSolutions(solutions, gi.hdt);

        assertEquals(gi.numpaths, solutions.size());
    }
}