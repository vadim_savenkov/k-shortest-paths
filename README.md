# k-shortest-paths

A collection of different algorithms for the top-k shortest path problem in RDF data.
The code contains a straight-forward breadth-first search algorithms and its bi-directional search version. 
Please note, that the algorithms currently require a HDT file of the RDF datasets.

# Reproduce the evaluation for the IESD'16 submission

1. Checkout the repository 
2. Download the training datasets in HDT from the download folder from this bitbucket account into the root of the project
   The evaluation dataset (805MB) in HDT format can be obtained from  https://ai.wu.ac.at/owncloud/index.php/s/3cz1rg7JXzBvn3y (Password: kpaths)

   Alternatively, the original datasets can be obtained from the ESWC Challenge repository: https://bitbucket.org/ipapadakis/eswc2016-challenge/downloads
   
   HDT conversion tool: https://code.google.com/archive/p/hdt-it/downloads

3. Experiment1: run on linux #> sh experiment1.sh
    this will run the tasks defined in benchmark 10 times over the training and evaluation datasets
4. Experiment 2:
   Just run the Start.java class in the root